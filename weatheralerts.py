#!/usr/bin/env python3
'''custom weather alerts'''
import time
import datetime
import math
import json

import asyncio
import aiohttp


# TODO put in a config file
LOCATION = 'Sydney,AU'
UNITS = 'metric'
ALERT_HORIZON_DAYS = 2
FORECAST_HORIZON_INTERVALS = 2


BASE_WEATHER_API = 'https://api.openweathermap.org/data/2.5/'
FORECAST_QUERY = f'forecast?q={LOCATION}&units={UNITS}'
CURRENT_QUERY = f'weather?q={LOCATION}&units={UNITS}'
with open('apikey', 'rt', encoding='utf8') as api_key_handle:
    API_KEY = api_key_handle.read().strip()
API_KEY_PARAM = f'&appid={API_KEY}'


ANSI_RED = '\033[91m'
ANSI_RESET = '\033[0m'
ANSI_BOLD = '\033[1m'


def load_json(filename: str) -> dict:
    '''loads and parses a json file'''
    with open(filename, 'rt', encoding='utf8') as json_file:
        return json.load(json_file)


ALERTS = load_json('alerts.json')
CURRENT_DISPLAY = load_json('current.json')


def calc_absolute_humidity(forecast: dict) -> float:
    '''given a forecast, calculates the absolute humidity in grams per cubic meter'''
    main_forecast = forecast['main']
    humidity = main_forecast['humidity'] / 100
    temperature_k = main_forecast['temp'] + 273.15

    critical_pressure_k = 22.064
    critical_temperature_k = 647.096

    tau = 1 - temperature_k / critical_temperature_k
    exp = critical_temperature_k / temperature_k * (
        -7.85951783 * tau +
        1.84408259 * tau ** 1.5 +
        -11.7866497 * tau ** 3 +
        22.6807411 * tau ** 3.5 +
        -15.9618719 * tau ** 4 +
        1.80122502 * tau ** 7.5
    )
    saturation_vapor_pressure_mpa = critical_pressure_k * math.exp(exp)
    vapor_pressure_pa = saturation_vapor_pressure_mpa * 1_000_000 * humidity
    absolute_humidity_g_m3 = vapor_pressure_pa / (461.5 * temperature_k) * 1000
    return round(absolute_humidity_g_m3, 2)


CALCULATED_PROPERTIES = {
    'calc': {
        'absolute_humidity': calc_absolute_humidity
    },
}


async def make_api_query(get_query: str) -> dict:
    '''makes API query with location, api key and provided query'''
    async with aiohttp.request('GET', BASE_WEATHER_API + get_query + API_KEY_PARAM) as response:
        if response.status != 200:
            print('API did not return HTTP OK 200, retured:', response.status, await response.text())
            raise NotImplementedError
        return await response.json()


async def get_forecast() -> dict:
    '''get forecast data from api'''
    return await make_api_query(FORECAST_QUERY)


async def get_current() -> dict:
    '''get current weather data from api'''
    return await make_api_query(CURRENT_QUERY)


def calc_properties(forecast: dict, template: dict) -> dict:
    '''recursive property calculator'''
    calculated_properties = {}
    for name, rule in template.items():
        if isinstance(rule, dict):
            calculated_properties[name] = calc_properties(forecast, rule)
        elif callable(rule):
            calculated_properties[name] = rule(forecast)
        else:
            raise TypeError(rule)
    return calculated_properties


def add_calc_properties(forecast: dict) -> dict:
    '''combines forecast with calculated properties'''
    forecast.update(calc_properties(forecast, CALCULATED_PROPERTIES))
    return forecast


def process_forecast(forecast_response: dict) -> tuple:
    '''parse forecast data and process alerts'''
    aggregated_alerts = {}
    aggregated_forecasts = {}
    for forecast in forecast_response['list']:
        forecast = add_calc_properties(forecast)
        triggered_alerts = get_alerts(forecast, ALERTS)
        aggregated_forecasts[forecast['dt']] = process_individual_forecast(forecast)[1]
        if triggered_alerts:
            aggregated_alerts[forecast['dt']] = triggered_alerts
    return aggregated_alerts, aggregated_forecasts


def process_individual_forecast(forecast: dict) -> tuple:
    '''processes individual forecast'''
    timestamp = forecast['dt']
    parsed_forecast = parse_forecast(add_calc_properties(forecast), CURRENT_DISPLAY)
    return (timestamp, parsed_forecast)


def get_alerts(forecast: dict, alerts: dict) -> dict:
    '''recursive alert parser'''
    triggered_alerts = {}
    for name, rule in alerts.items():
        if isinstance(rule, dict):
            triggered_alerts.update(get_alerts(forecast[name], rule))
        elif isinstance(rule, list):
            comparator = rule[0]
            value = rule[1]
            if comparator not in ('<', '>'):
                raise NotImplementedError
            data = forecast[name]
            if comparator == '>' and data > value \
            or comparator == '<' and data < value:
                triggered_alerts[name] = data
        else:
            raise TypeError(rule)
    return triggered_alerts


def parse_forecast(current_part: dict, template: dict) -> dict:
    '''recursive current weather parser'''
    parsed_forecast = {}
    for name, value in template.items():
        if isinstance(value, dict):
            next_part = current_part.get(name)
            if next_part:
                parsed_forecast.update(parse_forecast(current_part[name], value))
        elif isinstance(value, str):
            parsed_part = current_part.get(name)
            if parsed_part:
                parsed_forecast[value] = parsed_part
        else:
            raise TypeError(value)
    return parsed_forecast


def display_current(current: tuple) -> None:
    '''prints the current weather in nice human readable format'''
    timestamp, current_data = current
    human_time = time.strftime('%H:%M:%S', time.localtime(timestamp))
    print(f'{ANSI_BOLD}Current weather at {human_time} for {LOCATION}:{ANSI_RESET}')
    for name, value in current_data.items():
        print(f'\t{name}: {value}')
    print()


def display_alerts(alerts: dict) -> None:
    '''prints the aggregated alerts in nice human readable format'''
    now = datetime.datetime.now()
    alerts = {k: v for k, v in alerts.items() if (datetime.datetime.fromtimestamp(k) - now).days + 1 <= ALERT_HORIZON_DAYS}
    if not alerts:
        return
    print(f'{ANSI_BOLD}Upcoming weather alerts detected ({ALERT_HORIZON_DAYS} day horizon):{ANSI_RESET}')
    now = datetime.datetime.now()
    for timestamp, forecast_alerts in sorted(alerts.items(), key=lambda i: i[0]):
        time_string = time.strftime('%a %d %b %H:%M: ', time.localtime(timestamp))
        for alert_name, alert_value in forecast_alerts.items():
            print(f'\t{ANSI_RED}(!){ANSI_RESET} {time_string}{alert_name}: {alert_value}')
            if time_string[0] != ' ':
                time_string = ' ' * len(time_string)
    print()


def display_next_forecast(forecasts: dict) -> None:
    '''prints the next forecasts as defined by configuration'''
    for timestamp, forecast in sorted(forecasts.items(), key=lambda i: i[0])[:FORECAST_HORIZON_INTERVALS]:
        time_string = time.strftime('%H:%M', time.localtime(timestamp))
        print(f'{ANSI_BOLD}Forecast at {time_string}:{ANSI_RESET}')
        for name, value in forecast.items():
            print(f'\t{name}: {value}')
        print()


async def make_queries() -> dict:
    '''asyncronously perform all the api queries for weather data'''
    queries = {
        'forecast': get_forecast,
        'current': get_current
    }
    workers = {}
    for name, aio in queries.items():
        workers[name] = asyncio.create_task(aio())
    await asyncio.wait(workers.values())
    data = {}
    for name, worker in workers.items():
        data[name] = worker.result()
    return data


async def main() -> None:
    '''entry point'''
    queries = await make_queries()
    display_current(process_individual_forecast(queries['current']))
    alerts, forecasts = process_forecast(queries['forecast'])
    display_alerts(alerts)
    display_next_forecast(forecasts)


if __name__ == '__main__':
    asyncio.run(main())
